package midw

import "github.com/gin-gonic/gin"

// AuthHandler is a gin middleware that supports authenticate the request
// set correct user id, roles, etc into context for next handlers
// atm we assume to trust all incoming data (x-user-id, x-role)
// for real usage, we should validate & obtains them from JWT token
func AuthHandler(c *gin.Context) {
	userID := c.GetHeader("x-user-id")
	// role := c.GetHeader("x-role")
	if userID == "" {
		c.AbortWithStatusJSON(401, gin.H{"error": "unauthenticated"})
		return
	}

	c.Set("x-user-id", userID)
	// c.Set("x-role", role)

	c.Next()
}
